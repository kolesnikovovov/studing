#[macro_use] extern crate rocket;
use dotenv::dotenv;
use envy::from_env;
#[macro_use] extern crate lazy_static;
#[macro_use] extern crate serde_derive;

#[derive(Debug, Deserialize)]
pub struct Config {
    pub work_dir: String,
}

fn initialize_config() -> Config {
    dotenv().ok();

    from_env().expect("could not load config")
}

lazy_static! {
    pub static ref CONFIG: Config = initialize_config();
}

#[get("/")]
fn index() -> &'static str {
    "Hello, world!"
}

#[launch]
fn rocket() -> _ {
    println!("Config {}", CONFIG);

    rocket::build().mount("/", routes![index])
}